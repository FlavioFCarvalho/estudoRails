module ApplicationHelper
  def format_date(date)
    date.strftime("%d/%m/%Y")
  end

  def format_hour(hour)
    hour.strftime("%H:%M")
  end
end
