jQuery ->
  $('#competidores_evento_competidor_id').parent().hide()
  competidores = $('#competidores_evento_competidor_id').html()
  $('#competidores_evento_categoria_id').change ->
    categorias = $('#competidores_evento_categoria_id :selected').text()
    escaped_category = categorias.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(competidores).filter("optgroup[label='#{escaped_category}']").html()
    if options
      $('#competidores_evento_competidor_id').html(options)
      $('#competidores_evento_competidor_id').parent().show()
    else
      $('#competidores_evento_competidor_id').empty()
      $('#competidores_evento_competidor_id').parent().hide()