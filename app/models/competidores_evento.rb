class CompetidoresEvento < ApplicationRecord
  belongs_to :evento
  belongs_to :partida
  belongs_to :competidor

  attr_accessor :categoria_id
end
