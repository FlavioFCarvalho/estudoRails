class Competidor < ApplicationRecord
  has_attached_file :foto, styles: { :medium => "200x200#" }
  validates_attachment_content_type :foto, content_type: /\Aimage\/.*\Z/
  belongs_to :categoria
  has_and_belongs_to_many :eventos

  def categoria_nome
    categoria.nome
  end

  def descricao
    "#{nome} - #{categoria_nome}"
  end
end
