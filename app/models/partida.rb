class Partida < ApplicationRecord
  validates :tipo, :tempo, :quantidade_rodadas, presence: true

  belongs_to :evento, class_name: "Evento", foreign_key: "evento_id"

  def to_s
    "#{tipo} - #{tempo}"
  end

  def addEvento=(resource)
    self.evento=resource
  end
end
