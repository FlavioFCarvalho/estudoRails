class Evento < ApplicationRecord
  validates :nome, :dia, :hora, presence: true

  has_attached_file :imagem, styles: { medium: "500x590>", thumb: "300x300>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :imagem, content_type: /\Aimage\/.*\z/

  has_many :partidas
  has_and_belongs_to_many :competidores

  def possui_partidas
    try(:partidas).present?
  end
end
