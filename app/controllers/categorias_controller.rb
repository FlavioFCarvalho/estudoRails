class CategoriasController < ApplicationController
  before_action :find_categoria, only: [:show, :edit, :update, :destroy]
  def index
    @categorias = Categoria.all.order("nome ASC")
  end

  def show
    
  end

  def new
    @categoria = Categoria.new
  end

  def create

    @categoria = Categoria.new(categoria_params)
    if @categoria.save
      redirect_to @categoria, notice: "Categoria criada com sucesso"
    else
      render 'new'
    end
  end

  def update
    if @categoria.update(categoria_params)
      redirect_to @categoria
    else
      render 'edit'
    end
  end

  def destroy
    @categoria.destroy
    redirect_to categorias_path, notice: "Categoria excluída com sucesso"
  end

  private

  def categoria_params
    params.require(:categoria).permit(:nome, :status)
  end

  def find_categoria
     @categoria = Categoria.find(params[:id])
  end 
end
