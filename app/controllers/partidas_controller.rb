class PartidasController < ApplicationController
  before_action :find_evento

  def new
    @partida = Partida.new
    respond_to &:js
  end

  def create
    @partida=Partida.new(resource_params)
    @partida.addEvento=@evento
    @partida.save
    respond_to &:js
  end

  def start_game
    
  end

  private
    def find_evento
      @evento = Evento.find(params[:evento_id])
    end

    def resource_params
      params.require(:partida).permit(:tipo, :tempo, :quantidade_rodadas, :evento_id)
    end
end
