class EventosController < ApplicationController
  before_action :set_evento, only: [:show, :edit, :update, :destroy]

  def index
    @eventos = Evento.all
  end

  def show
  end

  def new
    @evento = Evento.new
  end

  def edit
  end

  def create
    @evento = Evento.new(resource_params)
    respond_to do |format|
      if @evento.save
        format.html { redirect_to @evento, notice: 'Evento was successfully created.' }
        format.json { render :show, status: :created, location: @evento }
      else
        format.html { render :new }
        format.json { render json: @evento.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @evento.update(resource_params)
        format.html { redirect_to @evento, notice: 'Evento was successfully updated.' }
        format.json { render :show, status: :ok, location: @evento }
      else
        format.html { render :edit }
        format.json { render json: @evento.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @evento.destroy
    respond_to do |format|
      format.html { redirect_to eventos_url, notice: 'Evento was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_evento
      @evento = Evento.find(params[:id])
    end

    def resource_params
      params.require(:evento).permit(:nome, :dia, :hora, :imagem, partidas_attributes:[:evento_id, :tipo, :tempo, :quantidade_rodadas, :_destroy])
    end
end
