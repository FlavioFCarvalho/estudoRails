class CompetidoresEventosController < PartidasController
  before_action :find_partida

  def new
    @competidor_evento = CompetidoresEvento.new
  end

  def create
    @competidor_evento = CompetidoresEvento.new(resource_params)
    @competidor_evento.save
    redirect_to evento_path(@evento)
  end

  def destroy
    
  end

  private
    def find_partida
      @partida = Partida.find(params[:partida_id])
    end

    def resource_params
      params.require(:competidores_evento).permit(:evento_id, :partida_id, :competidor_id)
    end
end
