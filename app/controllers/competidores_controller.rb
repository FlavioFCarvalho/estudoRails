class CompetidoresController < ApplicationController
  before_action :find_competidor, only: [:show, :edit, :update, :destroy]
  def index
    if params[:categoria].blank?
      @competidor = Competidor.all.order("created_at DESC")
    else
      @categoria_id = Categoria.find_by(nome: params[:categoria]).id
      @competidor = Competidor.where(categoria_id: @categoria_id).order("created_at DESC")
    end    
  end

  def show
    
  end

  def new
    @competidor  = Competidor.new
  end

  def create

    @competidor = Competidor.new(competidor_params)

    if @competidor.save
      redirect_to @competidor, notice: "Competidor criado com sucesso"
    else
      render 'new'
    end
  end

  def update
    if @competidor.update(competidor_params)
      redirect_to @competidor
    else
      render 'edit'
    end
  end

  def destroy
    @competidor.destroy
    redirect_to root_path, notice: "Competidor excluído com sucesso"
  end

  private

  def competidor_params
    params.require(:competidor).permit(:nome, :tempo_de_esporte, :patrocinio, :apoio, :instagram, :foto, :categoria_id)
  end

  def find_competidor
    @competidor = Competidor.find(params[:id])
  end
end
