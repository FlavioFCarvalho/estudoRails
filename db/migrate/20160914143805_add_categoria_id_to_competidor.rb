class AddCategoriaIdToCompetidor < ActiveRecord::Migration[5.0]
  def change
    add_column :competidores, :categoria_id, :integer
  end
end
