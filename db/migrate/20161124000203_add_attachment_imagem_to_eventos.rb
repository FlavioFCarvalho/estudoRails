class AddAttachmentImagemToEventos < ActiveRecord::Migration
  def self.up
    change_table :eventos do |t|
      t.attachment :imagem
    end
  end

  def self.down
    remove_attachment :eventos, :imagem
  end
end
