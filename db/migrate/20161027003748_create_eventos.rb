class CreateEventos < ActiveRecord::Migration[5.0]
  def change
    create_table :eventos do |t|
      t.string :nome
      t.date :dia
      t.time :hora

      t.timestamps
    end
  end
end
