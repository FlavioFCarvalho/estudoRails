class AddAdministradorToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :administrador, :boolean
  end
end
