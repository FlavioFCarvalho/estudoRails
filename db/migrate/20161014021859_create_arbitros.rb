class CreateArbitros < ActiveRecord::Migration[5.0]
  def change
    create_table :arbitros do |t|
      t.string :nome

      t.timestamps
    end
  end
end
