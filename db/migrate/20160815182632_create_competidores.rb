class CreateCompetidores < ActiveRecord::Migration[5.0]
  def change
    create_table :competidores do |t|
      t.string :nome
      t.string :tempo_de_esporte
      t.text :patrocinio
      t.text :apoio
      t.string :instagram

      t.timestamps
    end
  end
end
