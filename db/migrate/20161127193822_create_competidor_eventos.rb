class CreateCompetidorEventos < ActiveRecord::Migration[5.0]
  def change
    create_table :competidores_eventos do |t|
      t.belongs_to :evento, foreign_key: true
      t.belongs_to :partida, foreign_key: true
      t.belongs_to :competidor, foreign_key: true

      t.timestamps
    end
  end
end
