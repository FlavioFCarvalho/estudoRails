class AddAttachmentFotoToCompetidores < ActiveRecord::Migration
  def self.up
    change_table :competidores do |t|
      t.attachment :foto
    end
  end

  def self.down
    remove_attachment :competidores, :foto
  end
end
