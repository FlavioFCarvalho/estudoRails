class CreatePartidas < ActiveRecord::Migration[5.0]
  def change
    create_table :partidas do |t|
      t.string :tipo
      t.string :tempo
      t.integer :quantidade_rodadas
      t.integer :evento_id

      t.timestamps
    end
  end
end
