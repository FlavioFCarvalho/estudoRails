Rails.application.routes.draw do
  get 'partida_competidor/new'

  resources :eventos do
    resources :partidas do
      resources :competidores_eventos
    end
  end

  resources :arbitros
  devise_for :users, skip:[:resgistrations]
  resources :competidores
  resources :categorias 
  resources :competicoes
  resources :users
              
  root "competidores#index"
 end
