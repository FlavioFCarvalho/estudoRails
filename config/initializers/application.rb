Rails.application do |config|
  config.active_record.time_zone_aware_types = [:datetime, :time]
  config.generators do |g|
    g.assets         false
    g.helper         false
    g.test_framework false
  end
end